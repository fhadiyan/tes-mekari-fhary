require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome

Given("I navigate to Amazon homepage") do
  driver.navigate.to "https://amazon.com/"
  sleep(2)
end

Then("I click Hello Sign In Button") do
  driver.find_element(:xpath,"*//a[@id='nav-link-accountList']").click
  sleep(2)
end

Given("I enter my Email") do
  driver.find_element(:xpath,"*//input[@id='ap_email']").send_keys('your-email')
  sleep(2)
end

Then("I click Continue") do
  driver.find_element(:xpath,"*//span[@id='continue']").click
  sleep(2)
end

And("I enter my Password") do
  driver.find_element(:xpath,"*//input[@id='ap_password']").send_keys('your-password')
  sleep(2)
end

Then("I click Sign In") do
  driver.find_element(:xpath,"*//span[@id='auth-signin-button']").click
  sleep(2)
end
