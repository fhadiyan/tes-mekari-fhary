require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome

Given("I navigate to Amazon homepage") do
	driver.navigate.to "https://amazon.com/"
  sleep(2)
end

Then("I click on Sign In button") do
 driver.find_element(:xpath,"*//a[@id='nav-link-accountList']").click
	sleep(2)
end

Given("I click on Create Account") do
	driver.find_element(:xpath,"*//span[@id='auth-create-account-link']").click
	sleep(2)
end

Then("I enter my Name") do
	driver.find_element(:xpath,"*//input[@id='ap_customer_name']").send_keys('your-name')
	sleep(2)
end

And("I enter my Email") do
	driver.find_element(:xpath,"*//input[@id='ap_email']").send_keys('your-email')
	sleep(2)
end

And("I enter my Password") do
	driver.find_element(:xpath,"*//input[@id='ap_password']").send_keys('your-password')
	sleep(2)
end

And("I Re-enter my Password") do
	driver.find_element(:xpath,"*//input[@id='ap_password_check']").send_keys('your-password')
	sleep(2)
end

And("I click on Create New Account") do
	driver.find_element(:xpath,"*//input[@id='continue']").click
	sleep(2)
end
